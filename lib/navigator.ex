defmodule Navigator do
  @max_breadth 2

  @moduledoc """
  Navigates the filesystem
  """

  @doc """

  """
  def navigate(dir) do
    expanded_dir = Path.expand(dir)
    go_through([expanded_dir], 0)
  end

  defp go_through([], _), do: nil

  defp go_through(_, breadth_remaining) when breadth_remaining > @max_breadth, do: nil

  defp go_through([content | rest], breadth_remaining) do
    print_and_navigate(content, File.dir?(content))
    go_through(rest, breadth_remaining + 1)
  end

  defp print_and_navigate(_dir, false), do: nil

  defp print_and_navigate(dir, true) do
    IO.puts(dir)
    children_dirs = File.ls!(dir)
    go_through(expand_dirs(children_dirs, dir), 0)
  end

  defp expand_dirs([], _relative_to), do: []

  defp expand_dirs([dir | dirs], relative_to) do
    expanded_dir = Path.expand(dir, relative_to)
    [expanded_dir | expand_dirs(dirs, relative_to)]
  end
end
