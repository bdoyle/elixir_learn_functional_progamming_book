defmodule ScrewsFactory do
  @moduledoc """
  Screws stuff
  """

  def run(pieces, :slow) do
    pieces
    |> Enum.map(&add_thread/1)
    |> Enum.map(&add_head/1)
    |> Enum.each(&output/1)
  end

  def run(pieces, :fast) do
    pieces
    |> Stream.map(&add_thread/1)
    |> Stream.map(&add_head/1)
    |> Enum.each(&output/1)
  end

  def run(pieces, :faster) do
    pieces
    |> Stream.chunk(50)
    |> Stream.flat_map(&add_thread_multiple/1)
    |> Stream.chunk(100)
    |> Stream.flat_map(&add_head_multiple/1)
    |> Stream.chunk(30)
    |> Stream.flat_map(&add_packaging/1)
    |> Enum.each(&output/1)
  end

  defp add_thread_multiple(pieces) do
    Process.sleep(50)
    Enum.map(pieces, &(&1 <> "--"))
  end

  defp add_head_multiple(pieces) do
    Process.sleep(100)
    Enum.map(pieces, &("o" <> &1))
  end

  defp add_packaging(pieces) do
    Process.sleep(70)
    Enum.map(pieces, &("|" <> &1 <> "|"))
  end

  defp add_thread(piece) do
    Process.sleep(50)
    piece <> "--"
  end

  defp add_head(piece) do
    Process.sleep(100)
    "o" <> piece
  end

  defp output(screw) do
    IO.inspect(screw)
  end
end
