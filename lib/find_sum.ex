defmodule FindSum do
  @doc """
  Given a list and a number n, return two numbers in the
  array that add up to n. If two numbers cannot be found
  in the array, return nil.
  iex> FindSum.find([], 0)
  nil

  iex> FindSum.find([1, 2, 4], 5)
  [1, 4]

  iex> FindSum.find([1, 2, 4], 7)
  nil
  """

  def find(list, total) when is_list(list) do
    list
    |> Enum.reduce_while(nil, fn x, acc ->
      diff = total - x

      if Enum.member?(list, diff) do
        {:halt, [x, diff]}
      else
        {:cont, acc}
      end
    end)
  end
end
