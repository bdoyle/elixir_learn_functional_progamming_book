defmodule SantaStuff do
  @moduledoc """
  Santa is trying to deliver presents in a large apartment building, but he can't find the 
  right floor - the directions he got are a little confusing. He starts on the ground floor 
  (floor 0) and then follows the instructions one character at a time.

  An opening parenthesis, (, means he should go up one floor, and a closing parenthesis, ), 
  means he should go down one floor.

  iex> SantaStuff.run("(()")
  1

  iex> SantaStuff.run("()")
  0

  iex> SantaStuff.run("((()((()")
  4
  """
  def run(input) when is_binary(input) do
    input
    |> String.codepoints()
    |> Enum.reduce(0, fn x, acc ->
      case x do
        "(" -> 1 + acc
        ")" -> -1 + acc
        _ -> raise "Invalid input #{x}, must be ( or )"
      end
    end)
  end
end
