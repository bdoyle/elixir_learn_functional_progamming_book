defmodule ElixirFunctionalPrograming do
  @moduledoc """
  Documentation for ElixirFunctionalPrograming.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ElixirFunctionalPrograming.hello
      :world

  """
  def hello do
    :world
  end
end
