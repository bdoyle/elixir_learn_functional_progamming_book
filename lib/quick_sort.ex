defmodule QuickSort do
  @moduledoc """
  Sorts stuff quickly
  """

  @doc """
  Sorts a list of arrays.

  iex> QuickSort.sort([1, 0, 1, 2, 3])
  [0, 1, 1, 2, 3]

  iex> QuickSort.sort([1])
  [1]

  iex> QuickSort.sort([1, 0])
  [0, 1]
  """
  def sort([]), do: []
  def sort([a]), do: [a]

  def sort([pivot | tail]) do
    {less_than, greater_than} = Enum.split_with(tail, &(&1 <= pivot))
    sort(less_than) ++ [pivot] ++ sort(greater_than)
  end
end
