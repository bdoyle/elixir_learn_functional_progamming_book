defmodule Sorting do
  @moduledoc """
  Sorts stuff
  """

  @doc """
  Given a list [3, 4, 2] returns [4, 3, 2]

  iex> Sorting.descending([8, 3, 4, 2, 6])
  [8, 6, 4, 3, 2]

  iex> Sorting.descending([1, 7, 5, 7, 8])
  [8, 7, 7, 5, 1]

  iex> Sorting.descending([1])
  [1]

  iex> Sorting.descending([])
  []
  """
  def descending([]), do: []
  def descending([a]), do: [a]

  def descending(list) do
    split_with = div(Enum.count(list), 2)
    {list_a, list_b} = Enum.split(list, split_with)
    merge(descending(list_a), descending(list_b), [])
  end

  @doc """
  Given a list [3, 7, 1], returns 1
  """
  def max([]), do: nil
  def max([only]), do: only

  def max([first, second | rest]) when first >= second do
    find_max(rest, first)
  end

  def max([first, second | rest]) when second > first do
    find_max(rest, second)
  end

  defp find_max([], max), do: max

  defp find_max([head | rest], max) when head >= max do
    find_max(rest, head)
  end

  defp find_max([_ | rest], max) do
    find_max(rest, max)
  end

  # Sorts two lists [7, 1], [7, 5], [] returns [7, 5, 5, 1]
  defp merge(list_a, [], merged_list), do: merged_list ++ list_a
  defp merge([], list_b, merged_list), do: merged_list ++ list_b

  defp merge([head_a | tail_a], list_b = [head_b | _], merged_list)
       when head_a >= head_b do
    merge(tail_a, list_b, merged_list ++ [head_a])
  end

  defp merge(list_a, [head_b | tail_b], merged_list) do
    merge(list_a, tail_b, merged_list ++ [head_b])
  end
end
