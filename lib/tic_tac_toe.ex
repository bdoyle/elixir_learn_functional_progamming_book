defmodule TicTacToe do
  @moduledoc """
  Given a tic tac toe input, determines the winner.
  """

  @doc """
  [ 
    :x, :x, :o,
    :x, :x, :o,
    :o, :o, :x
  ]

   iex> TicTacToe.winner({:x, :x, :o, :x, :x, :o, :o, :o, :x})
   {:winner, :x}

   iex> TicTacToe.winner({:x, :x, :o, :x, :x, :o, :o, :o, :o})
   {:winner, :o}

   iex> TicTacToe.winner({:x, :o, :o, :x, :o, :x, :x, :x, :o})
   {:winner, :x}

   iex> TicTacToe.winner({:x, :o, :o, :x, :o, :x, :o, :x, :o})
   {:winner, :o}

   iex> TicTacToe.winner({:x, :x, :o, :x, :o, :o, :x, :o, :o})
   {:winner, :x}

   iex> TicTacToe.winner({:x, :o, :o, :x, :o, :o, :o, :o, :x})
   {:winner, :o}

   iex> TicTacToe.winner({:x, :o, :x, :x, :o, :x, :o, :o, :x})
   {:winner, :x}

   iex> TicTacToe.winner({:x, :o, :x, :o, :o, :x, :x, :x, :o})
   {:no_winner}
  """
  # hortizontal top
  def winner({x, x, x, _, _, _, _, _, _}), do: {:winner, x}

  # hortizontal middle
  def winner({_, _, _, x, x, x, _, _, _}), do: {:winner, x}

  # hortizontal bottom
  def winner({_, _, _, _, _, _, x, x, x}), do: {:winner, x}

  # diagonal start top left
  def winner({x, _, _, _, x, _, _, _, x}), do: {:winner, x}

  # diagonal start top right
  def winner({_, _, x, _, x, _, x, _, _}), do: {:winner, x}

  # vertical left
  def winner({x, _, _, x, _, _, x, _, _}), do: {:winner, x}

  # vertical middle
  def winner({x, _, _, x, _, _, x, _, _}), do: {:winner, x}

  # vertical right
  def winner({_, _, x, _, _, x, _, _, x}), do: {:winner, x}

  # no winner
  def winner({_, _, _, _, _, _, _, _, _}), do: {:no_winner}
end
