defmodule BubbleSort do
  @moduledoc """
  Sorts stuff bubbly
  """

  @doc """
  Given an unsorted list, returns a sorted list.

  iex> BubbleSort.sort([1, 0, 2, 3, 0])
  [0, 0, 1, 2, 3, 4]

  iex> BubbleSort.sort([1, 0, 2, 3, 3])
  [0, 1, 2, 3, 3]
  """
  def sort(input) do
    sorted = bsort(input)

    # no more sorting
    if sorted == input do
      sorted
    else
      sort(sorted)
    end
  end

  defp bsort([a, b | tail]) when a > b do
    [b | bsort([a | tail])]
  end

  defp bsort([a, b | tail]) when b >= a do
    [a | bsort([b | tail])]
  end

  defp bsort(list), do: list
end
