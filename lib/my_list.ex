defmodule MyList do
  @moduledoc """
  Listy stuff
  """

  @doc """
  Given a list [3, 9, 8, 0] returns 9

  iex> MyList.max([3, 9, 8, 0])
  9

  iex> MyList.max([3, 0, 0, 0])
  3

  iex> MyList.max([0])
  0

  iex> MyList.max([])
  nil

  iex> MyList.max([-9, -8])
  -8
  """
  def max([]), do: nil
  def max([only]), do: only

  def max([first, second | rest]) when first >= second do
    find_max(rest, first)
  end

  def max([_, second | rest]) do
    find_max(rest, second)
  end

  defp find_max([], max), do: max

  defp find_max([first | rest], max) when first >= max do
    find_max(rest, first)
  end

  defp find_max([_ | rest], max) do
    find_max(rest, max)
  end

  @doc """
  Given a list [3, 9, 8, 0] returns 0

  iex> MyList.min([3, 9, 8, 0])
  0

  iex> MyList.min([3, 9, 3, 8])
  3

  iex> MyList.min([3, 9, -34322, 8])
  -34322

  iex> MyList.min([2])
  2

  iex> MyList.min([])
  nil
  """
  def min([]), do: nil

  def min([only]), do: only

  def min([first, second | rest]) when first <= second do
    find_min(rest, first)
  end

  def min([_, second | rest]) do
    find_min(rest, second)
  end

  defp find_min([], min), do: min

  defp find_min([first | rest], min) when first <= min do
    find_min(rest, first)
  end

  defp find_min([_ | rest], min) do
    find_min(rest, min)
  end
end
