defmodule EncounterShop do
  @enchanter_name 'Edwin'

  @doc """
  Returns a list of enchanted items:

  title: enchanter_name + item.title
  price: item.price * 3
  magic: true

  iex> EncounterShop.enchant([%{magic: true}])
  [%{magic: true}]

  iex> EncounterShop.enchant([%{title: "Hey", price: 3}])
  [%{title: "Edwin's Hey", price: 9, magic: true}]
  """
  def enchant(items) do
    Enum.map(items, &enchant_item/1)
  end

  defp enchant_item(item = %{magic: true}), do: item

  defp enchant_item(item) do
    %{
      title: "#{@enchanter_name}'s #{item.title}",
      price: item.price * 3,
      magic: true
    }
  end

  def test_items do
    [
      %{title: 'Longsword', price: 50, magic: false},
      %{title: 'Healing Potion', price: 60, magic: true},
      %{title: 'Rope', price: 10, magic: false},
      %{title: "Dragon's Spear", price: 100, magic: true}
    ]
  end
end
