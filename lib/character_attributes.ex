defmodule CharacterAttributes do
  @doc """
  Returns an number equal to,
  3 times intelligence + 2 times dexterity + 2 times strength
  """
  def total_spent(%{strength: str, dexterity: dex, intelligence: int}) do
    str * 2 + dex * 2 + int * 3
  end
end
