defmodule Fibonacci do
  @doc """
  Returns a value of the fibonacci sequence for the given n.

  iex> Fibonacci.sequence(0)
  []

  iex> Fibonacci.sequence(1)
  [0]

  iex> Fibonacci.sequence(2)
  [0, 1]

  iex> Fibonacci.sequence(3)
  [0, 1, 1]

  iex> Fibonacci.sequence(4)
  [0, 1, 1, 2]
  """
  def sequence(n) do
    Stream.unfold({0, 1}, fn {n1, n2} -> {n1, {n2, n1 + n2}} end)
    |> Enum.take(n)
  end
end
