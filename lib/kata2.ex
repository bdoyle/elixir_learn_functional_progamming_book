defmodule Kata2 do
  @doc """
  Write a binary chop method that takes an integer search target and a
  sorted array of integers. It should return the integer index of the
  target in the array, or `-1` if the target is not in the array. The
  signature will logically be:

  chop(int, array_of_int)  -> int

  iex> Kata2.chop(8, [1, 2, 3, 5, 7, 8])
  5

  iex> Kata2.chop(0, [1, 2, 3, 5, 7, 8])
  -1

  iex> Kata2.chop(1, [1, 2, 3, 5, 7, 8])
  0

  iex> Kata2.chop(3, [1, 2, 3, 5, 7, 8])
  2

  iex> Kata2.chop(7, [1, 2, 3, 5, 7, 8])
  4

  iex> Kata2.chop(8, [8])
  0

  iex> Kata2.chop(8, [8, 9])
  0

  iex> Kata2.chop(8, [7, 8, 9])
  1
  """

  def chop(int, list) do
    chop_it(int, list, 0)
  end

  # int is not included in the list
  defp chop_it(_, [], _), do: -1

  # int is included in the list
  defp chop_it(int, [a], index) when a == int, do: index

  defp chop_it(int, list, index) do
    split_with = div(Enum.count(list), 2)
    {list_a, list_b} = Enum.split(list, split_with)

    if int >= List.first(list_b) do
      chop_it(int, list_b, index + Enum.count(list_a))
    else
      chop_it(int, list_a, index)
    end
  end
end
