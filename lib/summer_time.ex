defmodule SummerTime do
  @doc """
  Given number 5, returns 5 + 4 + 3 + 2 + 1

  iex> SummerTime.up_to(4)
  10

  iex> SummerTime.up_to(1)
  1

  iex> SummerTime.up_to(10)
  55

  """
  def up_to(number) when number > 0 do
    sum_up_to(number - 1, number)
  end

  defp sum_up_to(0, sum), do: sum

  defp sum_up_to(number, sum) do
    sum_up_to(number - 1, sum + number)
  end

  @doc """
  Returns a sum of the values in the list. 
  [5, 4, 3, 5] -> 17

  iex> SummerTime.sum([5])
  5

  iex> SummerTime.sum([5, 1])
  6

  iex> SummerTime.sum([5, 1, 0])
  6

  iex> SummerTime.sum([-5, 1, 0])
  -4
  """
  def sum([only]), do: only

  def sum([first | rest]) do
    sum_again(rest, first)
  end

  def sum_again([], sum), do: sum

  def sum_again([first | rest], sum) do
    sum_again(rest, sum + first)
  end
end
