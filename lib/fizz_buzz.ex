defmodule FizzBuzz do
  @moduledoc """
  Write a short program that prints each number 
  from 1 to 100 on a new line. 

  > For each multiple of 3, print "Fizz" instead of the number. 
  > For each multiple of 5, print "Buzz" instead of the number. 
  > For numbers which are multiples of both 3 and 5, 
  print "FizzBuzz" instead of the number.
  """

  def idiomatic(n) when rem(n, 15) == 0, do: "FizzBuzz"
  def idiomatic(n) when rem(n, 03) == 0, do: "Fizz"
  def idiomatic(n) when rem(n, 05) == 0, do: "Buzz"
  def idiomatic(n), do: n

  def case_type(n) do
    case {rem(n, 5), rem(n, 3)} do
      {0, 0} -> "FizzBuzz"
      {_, 0} -> "Fizz"
      {0, _} -> "Buzz"
      _ -> n
    end
  end

  @doc """
  iex> FizzBuzz.run(&FizzBuzz.idiomatic/1, 100000000)
  :ok

  iex> FizzBuzz.run(&FizzBuzz.case_type/1, 100000000)
  :ok
  """
  def run(fun, times) do
    Enum.map(1..times, fun)
    :ok
  end
end
