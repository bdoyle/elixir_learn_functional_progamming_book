defmodule Factorial do
  def of(0), do: 1
  def of(n), do: n * of(n - 1)

  @doc """
  Returns the factorial of the given number. Uses tail call recursion (I think).

  iex> Factorial.tail_call(0)
  1

  iex> Factorial.tail_call(1)
  1

  iex> Factorial.tail_call(2)
  2

  iex> Factorial.tail_call(6)
  720
  """
  def tail_call(n) do
    _tail_call(n, 1)
  end

  defp _tail_call(n, acc) when n < 2, do: acc

  defp _tail_call(n, acc) do
    _tail_call(n - 1, acc * n)
  end
end
